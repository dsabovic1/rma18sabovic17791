﻿Dženana Šabović   
17791  
2017/18  
RMA  

#Spirala 4

##Urađeno:

###Zadatak 1  
Ispravljen element liste listaKnjiga. Polja popunjena na odgovarajući način, za naslovnu sliku korišten picasso.

###Zadatak 2  
Dodano dugme dPreporuci koje otvara FragmentPreporuci. Klik na dugme dPosalji šalje odgovarajući email odaranom kontaktu. Korišten Contacts provider.

###Zadatak 3 
Napravljena baza sa potrebnim tabelama     
a) Nova kategorija se upisuje u bazu na opisani način. Korištena metoda dodajKategoriju.        

b) Nova knjiga se upisuje u bazu na opisani način. Korištena metoda dodajKnjigu.         

c) Klikom na kategoriju prikazuju se odgovarajuće knjige pomoću metode knjigeKategorije.        

d) Klikom na autora prikazuju se odgovarajuće knjige pomoću metode knjigeAutora.          


##Nije urađeno:

 