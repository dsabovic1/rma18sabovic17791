package ba.unsa.etf.rma.dzenana.mybooks;

import java.util.ArrayList;

/**
 * Created by Dyenana on 04/07/2018.
 */

public class Autor {

    private String imeiPrezime;
    private ArrayList<String> knjige;

    public int brojKnjiga() {
        return knjige.size();
    }

    public Autor(String imeiPrezime, String idKnjige) {
        this.imeiPrezime = imeiPrezime;
        knjige = new ArrayList<>();
        dodajKnjigu(idKnjige);
    }

    public void dodajKnjigu(String idKnjige) {
        //Provjera da li postoji
        for (String id: knjige) { //id vec u listi

            if (idKnjige!= null && id != null && id.compareTo(idKnjige)==0) { //== poredi na jednakost objekata?
                return; //vec postoji, ne dodajemo
            }
        }
        knjige.add(idKnjige);
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

}
