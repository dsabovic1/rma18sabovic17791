package ba.unsa.etf.rma.dzenana.mybooks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Dyenana on 05/24/2018.
 */

public class DBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "mojaBaza.db";
    public static final String DATABASE_TABLE_KATEGORIJA = "Kategorija";
    public static final String DATABASE_TABLE_KNJIGA = "Knjiga";
    public static final String DATABASE_TABLE_AUTOR = "Autor";
    public static final String DATABASE_TABLE_AUTORSTVO = "Autorstvo";
    public static final int DATABASE_VERSION = 2;
    public static final String KATEGORIJA_ID = "_id";
    public static final String KATEGORIJA_NAZIV = "naziv";
    public static final String KNJIGA_ID = "_id";
    public static final String KNJIGA_NAZIV = "naziv";
    public static final String KNJIGA_OPIS = "opis";
    public static final String KNJIGA_DATUM_OBJAVLJIVANJA = "datumObjavljivanja";
    public static final String KNJIGA_BR_STR = "brojStranica";
    public static final String KNJIGA_ID_WS = "idWebServis";
    public static final String KNJIGA_IDKATEGORIJE = "idkategorije";
    public static final String KNJIGA_SLIKA = "slika";
    public static final String KNJIGA_PREGLEDANA = "pregledana"; //1 jeste 0 nije
    public static final String AUTOR_ID = "_id";
    public static final String AUTOR_IME = "ime";
    public static final String AUTORSTVO_ID = "_id";
    public static final String AUTORSTVO_IDAUTORA = "idautora";
    public static final String AUTORSTVO_IDKNJIGE = "idknjige";

    //Tabela Kategorija
    private static final String CREATE_KATEGORIJA = "CREATE TABLE " + DATABASE_TABLE_KATEGORIJA + " ("+KATEGORIJA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+KATEGORIJA_NAZIV+" TEXT NOT NULL );";

    //Tabela Knjiga
    private static final String CREATE_KNJIGA = "CREATE TABLE " + DATABASE_TABLE_KNJIGA + " ("+KNJIGA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+KNJIGA_NAZIV+" TEXT, "+KNJIGA_OPIS+" TEXT, " +KNJIGA_DATUM_OBJAVLJIVANJA+" TEXT, " + KNJIGA_BR_STR+" INTEGER, " +KNJIGA_ID_WS+" TEXT,  " + KNJIGA_IDKATEGORIJE + " INTEGER NOT NULL, " + KNJIGA_SLIKA + " TEXT, " + KNJIGA_PREGLEDANA + " INTEGER );";

    //Tabela Autor
    private static final String CREATE_AUTOR = "CREATE TABLE " + DATABASE_TABLE_AUTOR + " ("+AUTOR_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+AUTOR_IME+" TEXT);";

    //Tabela Autorstvo
    private static final String CREATE_AUTORSTVO = "CREATE TABLE " + DATABASE_TABLE_AUTORSTVO + " ("+AUTOR_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+AUTORSTVO_IDAUTORA+" INTEGER NOT NULL, " +AUTORSTVO_IDKNJIGE+" INTEGER NOT NULL);";


    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Kreiramo sve tabele
        db.execSQL(CREATE_KATEGORIJA);
        db.execSQL(CREATE_KNJIGA);
        db.execSQL(CREATE_AUTOR);
        db.execSQL(CREATE_AUTORSTVO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTORSTVO);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KNJIGA);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KATEGORIJA);
        onCreate(db);
    }
}
