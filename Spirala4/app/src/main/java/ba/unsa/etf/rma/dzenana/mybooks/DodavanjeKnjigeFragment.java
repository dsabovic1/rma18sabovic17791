package ba.unsa.etf.rma.dzenana.mybooks;

import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.content.Context;

import java.io.FileDescriptor;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Dyenana on 04/05/2018.
 */

public class DodavanjeKnjigeFragment extends android.support.v4.app.Fragment {
    private static final int PICK_IMAGE = 100;
    Bitmap bm, icon;
    EditText imeAutora;
    EditText nazivKnjige;
    Spinner sKategorijaKnjige;
    ImageView naslovnaStrana;
    Uri imageUri;
    URL slika;

    DodavanjeKnjigeFragment.DodavanjeKnjigeFragmentListener acticityCommander;

    public interface DodavanjeKnjigeFragmentListener {
        void closeDodavanjeKnjgeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            acticityCommander = (DodavanjeKnjigeFragment.DodavanjeKnjigeFragmentListener) context;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dodavanje_knjige_fragment, container, false);

        imeAutora = (EditText) v.findViewById(R.id.imeAutora);
        nazivKnjige = (EditText) v.findViewById(R.id.nazivKnjige);

        sKategorijaKnjige = (Spinner) v.findViewById(R.id.sKategorijaKnjige);
        naslovnaStrana = (ImageView) v.findViewById(R.id.naslovnaStr);

        icon = BitmapFactory.decodeResource(getResources(), R.drawable.cover);
        naslovnaStrana.setImageBitmap(icon);

        //Kreirajmo adapter za spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, ListaObjekata.kategorije);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sKategorijaKnjige.setAdapter(adapter);

        Button dUpisiKnjigu = (Button) v.findViewById(R.id.dUpisiKnjigu);
        Button dNadjiSliku = (Button) v.findViewById(R.id.dNadjiSliku);
        Button dPonisti = (Button) v.findViewById(R.id.dPonisti);

        dUpisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (nazivKnjige.getText().length() == 0 || imeAutora.getText().length() == 0) {
                        Toast.makeText(getContext(), "Niste unijeli potrebne podatke!", Toast.LENGTH_LONG).show();
                        return; //Nije dozvoljen unos knjige bez naslova i autora - ako autor ne postoji unijeti N/A
                    }
                    Knjiga k = new Knjiga(nazivKnjige.getText().toString(), imeAutora.getText().toString(), sKategorijaKnjige.getSelectedItem().toString());
                    k.setSlika(slika);
                    if (bm != null) {
                        k.setNaslovnaStrana(bm);
                    } else {
                        //Default naslovna
                        k.setNaslovnaStrana(icon);
                    }

                    //Dodavanje knjige u bazu
                    dodajKnjigu(k);
                    Toast.makeText(getContext(), "Uspjesno ste dodali novu knjigu!", Toast.LENGTH_LONG).show();
                    nazivKnjige.setText("");
                    imeAutora.setText("");
                }
                catch (Exception e) {

                }
            }
        });

        dNadjiSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(); //MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i, "Odaberi sliku"), PICK_IMAGE);

            }
        });

        dPonisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                //Vracamo se na ListeFragment
                if (KategorijeAkt.siriL) {
                    closeDodavanjeKnjigeFragment(v);
                }
                else {
                    android.support.v4.app.FragmentManager fm;
                    android.support.v4.app.FragmentTransaction ft;
                    fm = getFragmentManager();
                    ft = fm.beginTransaction();
                    ListeFragment lf = new ListeFragment();

                    ft.replace(R.id.mjestoF1, lf);
                    ft.commit();
                }

            }
        });

        return v;
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    //Ova metoda će iz URI-a kojeg dobijete kao rezultat poziva intenta (getData()) vratiti Bitmap-u odabrane slike

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            //TODO: action
            imageUri = data.getData();
            try {
                slika = new URL (imageUri.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            naslovnaStrana.setImageURI(imageUri);
             try {
                 bm = getBitmapFromUri(imageUri);
             } catch (IOException e) {
                 e.printStackTrace();
             }
        }
    }

    public void closeDodavanjeKnjigeFragment(View v) {
        acticityCommander.closeDodavanjeKnjgeFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }

    public long dodajKnjigu(Knjiga k) {
        //Provjerimo da li postoji u tableli knjiga

        String[] koloneRezultat = new String[] {DBOpenHelper.KNJIGA_ID, DBOpenHelper.KNJIGA_NAZIV};
        String where = DBOpenHelper.KNJIGA_NAZIV + "= '" + k.getNaziv() + "'", whereArgs[] = null, groupBy = null, having = null, order = null;
        Cursor c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KNJIGA, koloneRezultat, where, whereArgs, groupBy, having, order);

        if (c.getCount()!=0) {
            Toast.makeText(getContext(), "Knjiga vec postoji u tabeli!", Toast.LENGTH_LONG).show();
            return -1;
        }
        c.close();

        //Knjiga ne postoji pa je dodajemo
        int idKategorije = 0;
        //Pronadjimo kategoriju
        String kategorija = k.getKategorija();
        Log.d("dodajKnjigu", kategorija);

        String[] koloneRezultatKat = new String[] {DBOpenHelper.KATEGORIJA_ID, DBOpenHelper.KATEGORIJA_NAZIV};
        String whereK = DBOpenHelper.KATEGORIJA_NAZIV + "= '" + kategorija + "'";
        Cursor cursor = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, koloneRezultatKat, whereK, whereArgs, groupBy, having, order);
        int INDEX = cursor.getColumnIndexOrThrow(DBOpenHelper.KATEGORIJA_ID);
        while (cursor.moveToNext()) {
            idKategorije = cursor.getInt(INDEX);
            Log.d("dodajKnjigu", Integer.toString(idKategorije));
        }



        ContentValues nova = new ContentValues();
        nova.put(DBOpenHelper.KNJIGA_IDKATEGORIJE, idKategorije);
        nova.put(DBOpenHelper.KNJIGA_ID_WS, k.getNaziv());
        nova.put(DBOpenHelper.KNJIGA_NAZIV, k.getNaziv());
        if (k.getSlika()!= null)
        nova.put(DBOpenHelper.KNJIGA_SLIKA, k.getSlika().toString());
        else   nova.put(DBOpenHelper.KNJIGA_SLIKA, "");

        if (k.isObojena()) nova.put(DBOpenHelper.KNJIGA_PREGLEDANA, 1);
        else nova.put(DBOpenHelper.KNJIGA_PREGLEDANA, 0);

        Log.d("dodajKnjigu1", k.getNaziv());
        long idKnjiga = KategorijeAkt.db.insert(DBOpenHelper.DATABASE_TABLE_KNJIGA, null, nova);
        Log.d("dodajKnjigu1", k.getNaziv());
        ListaObjekata.knjige.add(k); //Dodajemo u listu knjiga
        Log.d ("dodajKnjigu1", Integer.toString(ListaObjekata.knjige.size()));

        cursor.close();

        //Provjrimo da li postoji autor
        int i = 0;
        for (Autor a: k.getAutori()) {
            koloneRezultat = new String[] {DBOpenHelper.AUTOR_ID, DBOpenHelper.AUTOR_IME};
            where = DBOpenHelper.AUTOR_IME + "= '" + k.getAutori().get(i).getImeiPrezime() + "'";

            c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTOR, koloneRezultat, where, whereArgs, groupBy, having, order);

            if (c.getCount()==0) {
                //Dodajmo autora
                ContentValues novi = new ContentValues();
                novi.put(DBOpenHelper.AUTOR_IME, a.getImeiPrezime());
                long idAutor = KategorijeAkt.db.insert(DBOpenHelper.DATABASE_TABLE_AUTOR, null, novi);
                ListaObjekata.autori.add(new Autor(k.getAutori().get(i).getImeiPrezime(), k.getId())); //Popunimo listu autora

                //Popunimo tabelu autorstvo
                ContentValues noviA = new ContentValues();
                noviA.put(DBOpenHelper.AUTORSTVO_IDAUTORA, idAutor);
                noviA.put(DBOpenHelper.AUTORSTVO_IDKNJIGE, idKnjiga);
                KategorijeAkt.db.insert(DBOpenHelper.DATABASE_TABLE_AUTORSTVO, null, noviA);

            }

            i++;
        }
        c.close();
        return idKnjiga;
    }


}