package ba.unsa.etf.rma.dzenana.mybooks;


import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Dyenana on 05/12/2018.
 */
//Parametri ime i prezime autora
//vraca pet najnovijih knjiga autora

public class DohvatiNajnovije extends AsyncTask<String, Integer, Void> {
    ArrayList<Knjiga> rezKnjige=new ArrayList<Knjiga>();

    public interface IDohvatiNajnovijeDone{
        public void onDohvatiNajnovijeDone(ArrayList<Knjiga> rezKnjige);
    }
    private IDohvatiNajnovijeDone pozivatelj;

    public DohvatiNajnovije(IDohvatiNajnovijeDone p) {
        pozivatelj = p;
    }

    @Override
    protected Void doInBackground(String... strings) {
        String query = null;
        for(int i=0; i<strings.length; i++) {
            try {
                query = URLEncoder.encode(strings[i], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            //Formirajmo ispravan URL
            String url1 = "https://www.googleapis.com/books/v1/volumes?q=inauthor:" + query + "&maxResults=5&orderBy=newest";
            JSONArray items = new JSONArray();
            try {
                URL url = new URL(url1);
                //pozivanje web servisa
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = Konvertori.convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                items = jo.getJSONArray("items");
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            for (int j = 0; j < items.length(); j++) {
                if (j == 5) break;
                String id = null, naziv = null, opis = null, datum = null;
                ArrayList<Autor> autori = new ArrayList<Autor>();
                URL slika = null;
                String slikaString = null;
                int brStranica = 0;
                JSONArray autoriJSON = new JSONArray();
                try {
                    JSONObject item = items.getJSONObject(j);
                    id = item.getString("id");
                    JSONObject info = item.getJSONObject("volumeInfo");
                    naziv = info.getString("title");
                    opis = info.getString("description");
                    datum = info.getString("publishedDate");
                    autoriJSON = info.getJSONArray("authors");
                    JSONObject slikaLink = info.getJSONObject("imageLinks");
                    slikaString = slikaLink.getString("thumbnail");
                    slika = new URL(slikaString); //URL konstruktor
                    for (int k = 0; k < autoriJSON.length(); k++) {
                        String imeiPrezime = autoriJSON.getString(k);
                        autori.add(new Autor(imeiPrezime, id));
                    }
                    brStranica = info.getInt("pageCount");
                }
                catch (JSONException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                Knjiga nova = new Knjiga(id, naziv, autori, opis, datum, slika, brStranica);
                nova.setKategorija(FragmentOnline.kategorija);
                rezKnjige.add(nova);

            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onDohvatiNajnovijeDone(rezKnjige);
    }



}
