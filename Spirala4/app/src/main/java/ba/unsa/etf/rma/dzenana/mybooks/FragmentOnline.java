package ba.unsa.etf.rma.dzenana.mybooks;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by Dyenana on 05/12/2018.
 */

public class FragmentOnline extends android.support.v4.app.Fragment implements MyResultReceiver.Receiver, DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone {
    Button dRun, dPovratak, dAdd;
    EditText tekstUpit;
    Spinner sKategorije;
    Spinner sRezultat;
    ArrayList<Knjiga> knjigeRez;

    public static String kategorija = "Drama";

    FragmentOnlineListener acticityCommander;

    @Override
    public void onDohvatiDone(ArrayList<Knjiga> knjige) {
        if (knjige.size()==0) Toast.makeText(getContext(), "Knjige nisu pronađene", Toast.LENGTH_LONG).show();
        knjigeRez = knjige;
        for (int i=0; i<knjigeRez.size(); i++) {
            Log.d("Preuzte knjige", knjigeRez.get(i).getNaziv());
        }
        //Popunimo spinner sRezultat
        ArrayList<String> naziviRez = new ArrayList<String>();
        if (knjigeRez != null) {
            for (int i=0; i< knjigeRez.size(); i++) {
                naziviRez.add(knjigeRez.get(i).getNaziv());
            }
        }
        ArrayAdapter<String> a = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, naziviRez);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sRezultat.setAdapter(a);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch(resultCode) {
            case KnjigePoznanika.STATUS_START:
                break;
            case KnjigePoznanika.STATUS_FINISH:
                //Dohvatanje rezultata, update UI
                knjigeRez = resultData.getParcelableArrayList("resultData");
                //Popunimo spinner sRezultat
                ArrayList<String> naziviRez = new ArrayList<String>();
                if (knjigeRez != null) {
                    for (int i=0; i< knjigeRez.size(); i++) {
                        naziviRez.add(knjigeRez.get(i).getNaziv());
                    }
                }
                ArrayAdapter<String> a = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, naziviRez);
                a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sRezultat.setAdapter(a);
                break;
            case KnjigePoznanika.STATUS_ERROR:
                //Slucaj kada je doslo do greske
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onDohvatiNajnovijeDone(ArrayList<Knjiga> rezKnjige) {
        if (rezKnjige.size()==0) Toast.makeText(getContext(), "Knjige nisu pronađene", Toast.LENGTH_LONG).show();
        knjigeRez = rezKnjige;
        for (int i=0; i<knjigeRez.size(); i++) {
            Log.d("Preuzte knjige", knjigeRez.get(i).getNaziv());
        }
        //Popunimo spinner sRezultat
        ArrayList<String> naziviRez = new ArrayList<String>();
        if (knjigeRez != null) {
            for (int i=0; i< knjigeRez.size(); i++) {
                naziviRez.add(knjigeRez.get(i).getNaziv());
            }
        }
        ArrayAdapter<String> a = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, naziviRez);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sRezultat.setAdapter(a);

    }

    public interface FragmentOnlineListener {
        void closeFragmentOnline();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            acticityCommander = (FragmentOnline.FragmentOnlineListener) context;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.online_fragment, container, false);
        //Reference
        dRun = (Button) view.findViewById(R.id.dRun);
        dPovratak = (Button) view.findViewById(R.id.dPovratak);
        dAdd = (Button) view.findViewById(R.id.dAdd);
        tekstUpit = (EditText) view.findViewById(R.id.tekstUpit);
        sKategorije = (Spinner) view.findViewById(R.id.sKategorije);
        sRezultat = (Spinner) view.findViewById(R.id.sRezultat);

        //Kreirajmo adapter za spinner sKategorije
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, ListaObjekata.kategorije);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sKategorije.setAdapter(adapter);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        dPovratak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                tekstUpit.setText("");
                closeFragmentOnline();
            }
        });

        dAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sKategorije == null) {
                    Toast.makeText(getContext(), "Nije odabrana kategorija!", Toast.LENGTH_SHORT).show();
                    Log.d("dodajKnjigu", "sKategorije");
                    return;
                }
                //Nije odabrana knjiga
                if (sRezultat == null || sRezultat.getSelectedItem()==null) {
                    Toast.makeText(getContext(), "Nije odabrana knjiga!", Toast.LENGTH_SHORT).show();
                    Log.d("dodajKnjigu", "sRezultat");
                    return;
                }
                if (sKategorije.getSelectedItem() == null) {
                    Toast.makeText(getContext(), "Nije odabrana kategorija!", Toast.LENGTH_SHORT).show();
                    return;
                }
                kategorija = sKategorije.getSelectedItem().toString(); //Izbor kategorije
                Knjiga k;
                //Pronadjimo odabranu knjigu i dodajmo je u listu knjiga, dodajemo i autore
                for (int i=0; i< knjigeRez.size(); i++) { //Prolazimo kroz knjige iz spinnera
                    k = knjigeRez.get(i);
                    if (k.getNaziv().equalsIgnoreCase(sRezultat.getSelectedItem().toString())) {
                        k.setKategorija(kategorija);
                        /*Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.cover);
                        k.setNaslovnaStrana(icon);*/
                        long rez = dodajKnjigu(k);
                        if (rez!= -1)  {
                            Toast.makeText(getContext(), "Knjiga uspjesno dodana", Toast.LENGTH_SHORT).show();
                            Log.d("dodajKnjigu", "Knjiga dodana");
                        }
                        else {
                            Toast.makeText(getContext(), "Greska", Toast.LENGTH_SHORT).show();
                            Log.d("dodajKnjigu", "Greska");
                        }
                        //"Čistimo" formu
                        tekstUpit.setText("");
                        sRezultat.setAdapter(null);
                        break;
                    }
                }
            }
        });

        dRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tekst = tekstUpit.getText().toString();
                //Provjera uslova
                if (tekst.startsWith("autor:")) {
                    String param = tekst.replaceFirst("^autor:", "");
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone) FragmentOnline.this).execute(param);

                }
                else if (tekst.startsWith("korisnik:")) {
                    String param = tekst.replaceFirst("^korisnik:", "");
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    MyResultReceiver mReceiver = new MyResultReceiver(new Handler());
                    mReceiver.setReceiver(FragmentOnline.this);

                    intent.putExtra("idKorisnika", param); //Slanje podataka iz fragmenta intenServisu
                    intent.putExtra("receiver", mReceiver);
                    getActivity().startService(intent);
                }
                else if (tekst.contains(";")) { //slucaj 2 - vise stringova

                    String[] strings = tekst.split(Pattern.quote(String.valueOf(';')));

                    ArrayList<String> naziviRez = new ArrayList<String>();
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(strings);
                    for (String s : strings) {
                        Log.d("Preuzete knjige", s);
                    }


                }
                else /*if (!(tekst.contains(" ")))*/ { //slucaj 1 - jedna riječ upisana
                    //Toast.makeText(getContext(), "Pokrenut web servis", Toast.LENGTH_LONG).show();
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(tekst);

                }
            }
        });

    }

    public long dodajKnjigu(Knjiga k) {
        //Provjerimo da li postoji u tableli knjiga

        String[] koloneRezultat = new String[] {DBOpenHelper.KNJIGA_ID, DBOpenHelper.KNJIGA_NAZIV};
        String naziv = k.getNaziv().replace('\'', ' ');
        String where = DBOpenHelper.KNJIGA_NAZIV + "= '" + naziv + "'", whereArgs[] = null, groupBy = null, having = null, order = null;
        Cursor c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KNJIGA, koloneRezultat, where, whereArgs, groupBy, having, order);

        if (c.getCount()!=0) {
            Toast.makeText(getContext(), "Knjiga vec postoji u tabeli!", Toast.LENGTH_LONG).show();
            return -1;
        }
        c.close();

        //Knjiga ne postoji pa je dodajemo
        int idKategorije = 0;
        //Pronadjimo kategoriju
        String kategorija = k.getKategorija();
        Log.d("dodajKnjigu", kategorija);

        String[] koloneRezultatKat = new String[] {DBOpenHelper.KATEGORIJA_ID, DBOpenHelper.KATEGORIJA_NAZIV};
        String whereK = DBOpenHelper.KATEGORIJA_NAZIV + "= '" + kategorija + "'";
        Cursor cursor = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, koloneRezultatKat, whereK, whereArgs, groupBy, having, order);
        int INDEX = cursor.getColumnIndexOrThrow(DBOpenHelper.KATEGORIJA_ID);
        while (cursor.moveToNext()) {
            idKategorije = cursor.getInt(INDEX);
        }
        cursor.close();


        ContentValues nova = new ContentValues();
        nova.put(DBOpenHelper.KNJIGA_ID_WS, k.getId());
        nova.put(DBOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA, k.getDatumObjavljivanja());
        nova.put(DBOpenHelper.KNJIGA_IDKATEGORIJE, idKategorije);
        nova.put(DBOpenHelper.KNJIGA_NAZIV, k.getNaziv());
        nova.put(DBOpenHelper.KNJIGA_OPIS, k.getOpis());
        nova.put(DBOpenHelper.KNJIGA_BR_STR, k.getBrojStranica());
        if (k.getSlika()!= null)
        nova.put(DBOpenHelper.KNJIGA_SLIKA, k.getSlika().toString());
        if (k.isObojena()) nova.put(DBOpenHelper.KNJIGA_PREGLEDANA, 1);
        else nova.put(DBOpenHelper.KNJIGA_PREGLEDANA, 0);

        long idKnjiga = KategorijeAkt.db.insert(DBOpenHelper.DATABASE_TABLE_KNJIGA, null, nova);
        ListaObjekata.knjige.add(k); //Dodajemo u listu knjiga
        Log.d ("dodajKnjigu", Integer.toString(ListaObjekata.knjige.size()));

        //Provjrimo da li postoji autor
        int i = 0;
        for (Autor a: k.getAutori()) {
            koloneRezultat = new String[] {DBOpenHelper.AUTOR_ID, DBOpenHelper.AUTOR_IME};
            where = DBOpenHelper.AUTOR_IME + "= '" + k.getAutori().get(i).getImeiPrezime() + "'";

            c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTOR, koloneRezultat, where, whereArgs, groupBy, having, order);

            if (c.getCount()==0) {
                //Dodajmo autora
                ContentValues novi = new ContentValues();
                novi.put(DBOpenHelper.AUTOR_IME, a.getImeiPrezime());
                long idAutor = KategorijeAkt.db.insert(DBOpenHelper.DATABASE_TABLE_AUTOR, null, novi);
                ListaObjekata.autori.add(new Autor(k.getAutori().get(i).getImeiPrezime(), k.getId())); //Popunimo listu autora

                //Popunimo tabelu autorstvo
                ContentValues noviA = new ContentValues();
                noviA.put(DBOpenHelper.AUTORSTVO_IDAUTORA, idAutor);
                noviA.put(DBOpenHelper.AUTORSTVO_IDKNJIGE, idKnjiga);
                KategorijeAkt.db.insert(DBOpenHelper.DATABASE_TABLE_AUTORSTVO, null, noviA);

            }

            i++;
        }
        c.close();
        return idKnjiga;
    }



    public void closeFragmentOnline() {acticityCommander.closeFragmentOnline();}
}
