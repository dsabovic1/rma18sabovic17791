package ba.unsa.etf.rma.dzenana.mybooks;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import static android.Manifest.permission.READ_CONTACTS;

import java.util.ArrayList;
import java.util.HashSet;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * Created by Dyenana on 05/24/2018.
 */

public class FragmentPreporuci extends android.support.v4.app.Fragment {
    private static final int REQUEST_READ_CONTACTS = 0;
    Spinner sKontakti;
    TextView infoText;
    Button dPosalji, povratakButton;


    //Interfejs za komunikaciju sa mainActivity
    FragmentPreporuci.FragmentPreporuciListener activityCommander;

    public interface FragmentPreporuciListener {
        void closeFragmentPreporuci();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activityCommander = (FragmentPreporuci.FragmentPreporuciListener) context;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.preporuci_fragment, container, false);
        sKontakti = v.findViewById(R.id.sKontakti);
        infoText = v.findViewById(R.id.infoTextView);
        dPosalji = (Button) v.findViewById(R.id.dPosalji);
        povratakButton = (Button) v.findViewById(R.id.povratakButton);

        String info = "", autorK = "", nazivK = "", opisK = "", brojStr = "", kategorija = "" ;
        if (KategorijeAkt.kDetalji.getAutori()!= null && KategorijeAkt.kDetalji.getAutori().size()!=0)
            autorK = KategorijeAkt.kDetalji.getAutori().get(0).getImeiPrezime();
        if (KategorijeAkt.kDetalji.getNaziv()!= null)
            nazivK = KategorijeAkt.kDetalji.getNaziv();
        if (KategorijeAkt.kDetalji.getOpis()!= null)
            opisK = KategorijeAkt.kDetalji.getOpis();
        if (KategorijeAkt.kDetalji.getBrojStranica()> 0)
            brojStr = Integer.toString(KategorijeAkt.kDetalji.getBrojStranica());
        if (KategorijeAkt.kDetalji.getKategorija()!= null)
            nazivK = KategorijeAkt.kDetalji.getNaziv();

        info += "Naziv: " + nazivK + "\n";
        info += "Kategorija: " + kategorija + "\n";
        info += "Autor: " + autorK + "\n";
        info += "Opis: " + opisK + "\n";
        info += "Broj stranica: " + brojStr + "\n";

        infoText.setText(info);

        //Popunimo spinner sKontakti
        final ArrayList<Kontakt> kontakti= getContactList();
        ArrayList<String> emailAdrese = new ArrayList<>();

        for (Kontakt k: kontakti) {
            emailAdrese.add(k.getEmail());
        }

        ArrayAdapter<String> a = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, emailAdrese);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sKontakti.setAdapter(a);

        dPosalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Saljemo mail
                String sadrzaj = "", autor = "", naziv = "", ime = "", email = "";
                if (KategorijeAkt.kDetalji.getAutori()!= null && KategorijeAkt.kDetalji.getAutori().size()!=0)
                    autor = KategorijeAkt.kDetalji.getAutori().get(0).getImeiPrezime();
                if (KategorijeAkt.kDetalji.getNaziv()!= null)
                    naziv = KategorijeAkt.kDetalji.getNaziv();
                email = sKontakti.getSelectedItem().toString();
                for (Kontakt k: kontakti) {
                    if (k.getEmail().compareTo(email)==0) ime = k.getName();
                }

                sadrzaj = "Zdravo "+ ime +"," +"\nProcitaj knjigu " + naziv + " od " + autor + "!";

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
                i.putExtra(Intent.EXTRA_SUBJECT, "MyBooks");
                i.putExtra(Intent.EXTRA_TEXT   , sadrzaj);
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

                //Zatvaramo fragment
                Toast.makeText(getContext(), "Mail je poslan! Mail: " + sadrzaj, Toast.LENGTH_LONG).show();
                closeFragmentPreporuci();
            }
        });

        povratakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFragmentPreporuci();
            }
        }
        );



        return v;
    }

    private ArrayList<Kontakt> getContactList() {
        ArrayList<Kontakt> kontakti = new ArrayList<Kontakt>();
        ContentResolver cr = getActivity().getApplicationContext().getContentResolver();
        Cursor cur = null;
        try {
            cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);
        }
        catch (Exception e) {
            Log.e("Error in contacts", e.getMessage());
        }

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)); //Ime kontakta
                String email = "";

                Cursor eCur = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                if (eCur!= null && eCur.moveToFirst()) {
                    email = eCur.getString(eCur.getColumnIndex(
                            ContactsContract.CommonDataKinds.Email.DATA));
                    kontakti.add(new Kontakt(name, email));
                    Log.d("email", "Name: " + name);
                    Log.d("email", "Phone Number: " + email);
                }
                eCur.close();

            }
        }
        if(cur!=null){
            cur.close();
        }
        return kontakti;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void closeFragmentPreporuci(){activityCommander.closeFragmentPreporuci();}

}
