package ba.unsa.etf.rma.dzenana.mybooks;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import android.database.sqlite.SQLiteDatabase;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.DatagramChannel;

import static ba.unsa.etf.rma.dzenana.mybooks.ListaObjekata.autori;


public class KategorijeAkt extends AppCompatActivity implements ListAdapter.ListAdapterListener, ListeFragment.ListeFragmentListener, KnjigeFragment.KnjigeFragmentListener, DodavanjeKnjigeFragment.DodavanjeKnjigeFragmentListener, FragmentOnline.FragmentOnlineListener, FragmentPreporuci.FragmentPreporuciListener {
    android.support.v4.app.FragmentManager fm;
    android.support.v4.app.FragmentTransaction ft;
    public static SQLiteDatabase db;

    FrameLayout mjestoF1, mjestoF2, mjestoF3;
    ListeFragment lf;
    KnjigeFragment kf;
    DodavanjeKnjigeFragment dkf;
    FragmentOnline fo;
    FragmentPreporuci fp;

    public static String odabir;
    public static char c;
    public static Knjiga kDetalji;

    public static Boolean siriL = false;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);

        //Kreiranje baze podataka
        // Dohvatimo referencu na bazu (poslije ćemo opisati kako se implementira helper)
        DBOpenHelper helper = new DBOpenHelper(this);
        db = helper.getWritableDatabase();
        ListaObjekata.kategorije.clear();
        String where = null, whereArgs[] = null, groupBy = null, having = null, order = null;
        //Kupimo knjige i autore iz baze
        //Uzimamo knjige iz baze
        /*ListaObjekata.knjige.clear();

        String[] koloneRezultat = new String[] {DBOpenHelper.KATEGORIJA_ID, DBOpenHelper.KATEGORIJA_NAZIV};

        Cursor c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, koloneRezultat, where, whereArgs, groupBy, having, order);

        //Prolazimo kroz kategorije i uzimamo nazive i id
        int INDEX_NAZIV = c.getColumnIndexOrThrow(DBOpenHelper.KATEGORIJA_NAZIV);
        while (c.moveToNext()) {
            ListaObjekata.kategorije.add(c.getString(INDEX_NAZIV));
            Log.d("Kategorija:", c.getString(INDEX_NAZIV));
        }
        c.close();*/


        //Mehanizam dinamičkog ubacivanja fragmenata
        //dohvatanje FragmentManagera
        fm = getSupportFragmentManager();
        mjestoF1 = (FrameLayout) findViewById(R.id.mjestoF1);
        mjestoF2 = (FrameLayout) findViewById(R.id.mjestoF2);
        mjestoF3 = (FrameLayout) findViewById(R.id.mjestoF3);
        lf = new ListeFragment();
        kf = new KnjigeFragment();
        dkf = new DodavanjeKnjigeFragment();
        fo = new FragmentOnline();
        fp = new FragmentPreporuci();
        if (mjestoF2 != null) {
            //Siroki ekran
            siriL = true;
        }
        else {
            siriL = false;
        }
        addListeFragment();
    }

    public void onClickDodajKnjigu(View view) {
        addDodavanjeKnjigeFragment();
    }

    @Override
    public void addListeFragment() {
        //fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        lf = new ListeFragment();
        ft.replace(R.id.mjestoF1, lf);
        ft.show(lf);
        ft.commit();
    }

    @Override
    public void addKnjigeFragment(String o, char co) {
        odabir = o;
        c = co;
        ft = fm.beginTransaction();
        kf = new KnjigeFragment();
        if (siriL) {
            ft.replace(R.id.mjestoF2, kf);
            ft.show(kf);
        }
        else {
            ft.replace(R.id.mjestoF1, kf);
            //ft.addToBackStack(null);
        }
        ft.commit();
    }

    @Override
    public void addDodavanjeKnjigeFragment() {
        ft = fm.beginTransaction();
        if (siriL) {
           // Toast.makeText(getApplicationContext(), "siriL", Toast.LENGTH_LONG).show();
            //Trebamo sakriti fragmente 1 i 2
            if (lf!= null) {
                ft.hide(lf);
            }
            if (kf != null) {
                ft.hide(kf);
            }
            ft.replace(R.id.mjestoF3, dkf);
        }
        else {
            ft.replace(R.id.mjestoF1, dkf);
        }
        //ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void addOnlineFragment() {
        ft = fm.beginTransaction();
        if (siriL) {
            if (lf!= null) {
                ft.hide(lf);
            }
            if (kf != null) {
                ft.hide(kf);
            }
            ft.replace(R.id.mjestoF3, fo);
        }
        else {
            ft.replace(R.id.mjestoF1, fo);
        }
        ft.commit();
    }

    @Override
    public void openFragmentPreporuci(Knjiga k) {
        kDetalji = k;
        ft = fm.beginTransaction();
        if (siriL) {
            if (lf!= null) {
                ft.hide(lf);
            }
            if (kf != null) {
                ft.hide(kf);
            }
            ft.replace(R.id.mjestoF3, fp);
        }
        else {
            ft.replace(R.id.mjestoF1, fp);
        }
        ft.commit();
    }

    @Override
    public void closeKnjigeFragment() {
        if (siriL) {
            ft = fm.beginTransaction();
            ft.remove(kf);
            ft.commit();
        }
    }

    @Override
    public void closeDodavanjeKnjgeFragment() {
        ft = fm.beginTransaction();
        if (siriL) {
            ft.remove(dkf);
            ft.show(kf);
            ft.show(lf);
            ft.commit();
        }
        else {
            ft.replace(R.id.mjestoF1, lf);
            ft.commit();
        }


    }


    @Override
    public void closeFragmentOnline() {
        ft = fm.beginTransaction();
        if (siriL) {
            ft.remove(fo);
            ft.show(kf);
            ft.show(lf);
            ft.commit();
        }
        else {
            ft.replace(R.id.mjestoF1, lf);
            ft.commit();
        }
    }

    @Override
    public void closeFragmentPreporuci() {
        ft = fm.beginTransaction();
        if (siriL) {
            ft.remove(fo);
            ft.show(kf);
            ft.show(lf);
            ft.commit();
        }
        else {
            ft.replace(R.id.mjestoF1, kf);
            ft.commit();
        }
    }

}
