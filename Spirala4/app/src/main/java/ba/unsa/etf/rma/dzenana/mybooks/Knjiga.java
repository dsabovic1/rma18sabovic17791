package ba.unsa.etf.rma.dzenana.mybooks;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;


import java.util.ArrayList;

/**
 * Created by Dyenana on 03/26/2018.
 */

//Da bi prosllijedili listu knjiga iz fragmenta Knjiga mora biti parcelable, tj mora implementirati interfejs

public class Knjiga implements Parcelable {

    private String naziv, datumObjavljivanja, opis, id;
    private String kategorija;

    private Bitmap naslovnaStrana;

    private URL slika;
    private ArrayList<Autor> autori;
    private int brojStranica;

    private boolean obojena;
    //Moj konstruktor
    public Knjiga(String nazivKnjige, String imeAutora, String kategorija) {
        autori = new ArrayList<Autor>();
        this.autori.add(new Autor(imeAutora, nazivKnjige)); //imeIprezime, idKnjige
        this.naziv = nazivKnjige;
        this.kategorija = kategorija;
        this.id = nazivKnjige;
        obojena = false;
        /*String s = "https://books.google.ba/googlebooks/images/no_cover_thumb.gif";
        try {
            slika = new URL(s);
            Log.d("slikak1", slika.toString());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }*/
    }

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica) {
        this.naziv = naziv;
        this.datumObjavljivanja = datumObjavljivanja;
        this.opis = opis;
        this.id = id;
        this.autori = autori;
        this.brojStranica = brojStranica;
        if (slika == null) { //Default pozadina
            String s = "https://books.google.ba/googlebooks/images/no_cover_thumb.gif";
            try {
                slika = new URL(s);
                Log.d("slikak2", slika.toString());
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        else this.slika = slika;
        if (slika!= null) Log.d("slikak2", slika.toString());
        this.obojena = false;
        this.kategorija = "Bez kategorije";
        if(this.id == null) this.id= this.naziv;
    }


    //Getteri i setteri

    protected Knjiga(Parcel in) {
        naziv = in.readString();
        datumObjavljivanja = in.readString();
        opis = in.readString();
        id = in.readString();
        kategorija = in.readString();
        naslovnaStrana = in.readParcelable(Bitmap.class.getClassLoader());
        brojStranica = in.readInt();
        obojena = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeString(datumObjavljivanja);
        dest.writeString(opis);
        dest.writeString(id);
        dest.writeString(kategorija);
        dest.writeParcelable(naslovnaStrana, flags);
        dest.writeInt(brojStranica);
        dest.writeByte((byte) (obojena ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public boolean isObojena() {
        return obojena;
    }

    public void setObojena(boolean obojena) {
        this.obojena = obojena;
    }

    public Bitmap getNaslovnaStrana() {
        return naslovnaStrana;
    }
    public void setNaslovnaStrana(Bitmap naslovnaStrana) {
        this.naslovnaStrana = naslovnaStrana;
    }


}