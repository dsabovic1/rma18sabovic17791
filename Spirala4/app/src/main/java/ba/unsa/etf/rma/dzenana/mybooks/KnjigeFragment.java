package ba.unsa.etf.rma.dzenana.mybooks;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.content.Intent;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;

import static ba.unsa.etf.rma.dzenana.mybooks.KategorijeAkt.siriL;

/**
 * Created by Dyenana on 04/07/2018.
 */

public class KnjigeFragment extends android.support.v4.app.Fragment {
    ListView listaKnjiga;
    protected String odabir;
    protected char c;
    URL slikaURL;
    ArrayList<Knjiga> knjigeOdabir = new ArrayList<Knjiga>();

    KnjigeFragment.KnjigeFragmentListener acticityCommander;

    public interface KnjigeFragmentListener {
        void addListeFragment();
        void closeKnjigeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            acticityCommander = (KnjigeFragment.KnjigeFragmentListener) context;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.knjige_fragment, container, false);

        Button dPovratak = (Button) v.findViewById(R.id.dPovratak);

        dPovratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (siriL) {
                    //Zatvorimo ovaj fragement
                    closeKnjigeFragment(v);
                }
                openListeFragment(v);
            }
        });

        listaKnjiga = (ListView) v.findViewById(R.id.listaKnjiga);

        //Preuzimanje kateogrije
        odabir = KategorijeAkt.odabir;
        c = KategorijeAkt.c;

        //Lista knjiga po kategoriji
        if (c == 'k') {
            /*for (int j = 0; j<ListaObjekata.knjige.size(); j++) {
                if (ListaObjekata.knjige.get(j).getKategorija().equalsIgnoreCase(odabir)) {
                    knjigeOdabir.add(ListaObjekata.knjige.get(j));
                }
            }*/
            //Upit koji vraca knjige iz odabrane kategorije iz baze
            long idKategorije = 0;
            //Pronadjimo idKategorije
            String[] koloneRezultat = new String[] {DBOpenHelper.KATEGORIJA_ID, DBOpenHelper.KATEGORIJA_NAZIV};
            String where = DBOpenHelper.KATEGORIJA_NAZIV + "= '" + odabir + "'", whereArgs[] = null, groupBy = null, having = null, order = null;
            Log.d("kategorija", odabir);
            Cursor cur = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, koloneRezultat, where, whereArgs, groupBy, having, order);
            int INDEX_KOLONE_ID = cur.getColumnIndexOrThrow(DBOpenHelper.KATEGORIJA_ID);

            while (cur.moveToNext()) {
                idKategorije = cur.getLong(INDEX_KOLONE_ID);
            }
            cur.close();
            try {
                Log.d("indeks", Long.toString(idKategorije));
                knjigeOdabir = knjigeKategorije(idKategorije);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }



        //Lista knjiga autor
        if (c == 'a') {
            /*for (int j = 0; j<ListaObjekata.knjige.size(); j++) {
                ArrayList<Autor> a = ListaObjekata.knjige.get(j).getAutori();
                if (a.size() == 0 && odabir.equalsIgnoreCase("N/A")) { //Knjiga bez autora
                    knjigeOdabir.add(ListaObjekata.knjige.get(j));
                }
                for (int k=0; k< a.size(); k++) {
                    if (odabir.equalsIgnoreCase(a.get(k).getImeiPrezime())) {
                        knjigeOdabir.add(ListaObjekata.knjige.get(j));
                    }
                }

            }*/

            //Upit koji vraca knjige odabranog autora iz baze
            long idAutora = 0;
            //Pronadjimo idKAutora
            String[] koloneRezultat = new String[] {DBOpenHelper.AUTOR_ID, DBOpenHelper.AUTOR_IME};
            String where = DBOpenHelper.AUTOR_IME + "= '" + odabir +"'", whereArgs[] = null, groupBy = null, having= null, order= null;
            Cursor cur = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTOR, koloneRezultat, where, whereArgs, groupBy, having, order);
            int INDEX_KOLONE_ID = cur.getColumnIndexOrThrow(DBOpenHelper.AUTOR_ID);
            if (cur.getCount()==1) {
                while (cur.moveToNext()) {
                    idAutora = cur.getLong(INDEX_KOLONE_ID);
                }

            }
            else {
                return v;
            }
            cur.close();
            knjigeOdabir = knjigeAutora(idAutora);
        }

        ListAdapter adapter = new ListAdapter(getContext(), R.layout.element_liste, knjigeOdabir);
        listaKnjiga.setAdapter(adapter);


        listaKnjiga.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpterView, View view, int position, long id) {
                for (int i = 0; i < listaKnjiga.getChildCount(); i++) {
                    if(position == i ){
                        listaKnjiga.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.svijetloPlava));
                        //Knjiga koja je dodana ostaje obojena
                        knjigeOdabir.get(i).setObojena(true);

                        //Unesimo u bazu
                        String where = DBOpenHelper.KNJIGA_NAZIV + "= '" + knjigeOdabir.get(i).getNaziv() +"'", whereArgs[] = null;

                        ContentValues update = new ContentValues();
                        update.put(DBOpenHelper.KNJIGA_PREGLEDANA, 1);

                        KategorijeAkt.db.update(DBOpenHelper.DATABASE_TABLE_KNJIGA, update, where, whereArgs);

                    }
                }
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void openListeFragment(View v) {
        acticityCommander.addListeFragment();
    }
    public void closeKnjigeFragment(View v) {
        acticityCommander.closeKnjigeFragment();
    }

    ArrayList<Knjiga> knjigeKategorije(long idKategorije) throws MalformedURLException {
        ArrayList<Knjiga> rez = new ArrayList<>();

        //Upit vraca sve knjige odabrane kategorije iz baze
        String[] koloneRezultat = new String[] {DBOpenHelper.KNJIGA_ID, DBOpenHelper.KNJIGA_NAZIV, DBOpenHelper.KNJIGA_ID_WS, DBOpenHelper.KNJIGA_BR_STR, DBOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA, DBOpenHelper.KNJIGA_OPIS, DBOpenHelper.KNJIGA_IDKATEGORIJE, DBOpenHelper.KNJIGA_PREGLEDANA, DBOpenHelper.KNJIGA_SLIKA};
        String where = DBOpenHelper.KNJIGA_IDKATEGORIJE + "= " + idKategorije  , whereArgs[] = null, groupBy = null, having = null, order = null;
        Cursor cur = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KNJIGA, koloneRezultat, where, whereArgs, groupBy, having, order);

        int INDEX_ID_WS = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_ID_WS);
       int INDEX_NAZIV = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_NAZIV);
       int INDEX_OPIS = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_OPIS);
       int INDEX_DATUM = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA);
       int INDEX_BROJ = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_BR_STR);
       int INDEX = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_ID);
       int I_OBOJENA = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_PREGLEDANA);
       Log.d("index", Integer.toString(I_OBOJENA));
       int I_SLIKA = cur.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_SLIKA);
        //Kupimo podatke o knjizi
        while (cur.moveToNext()) {
            String idws = cur.getString(INDEX_ID_WS);
            String naziv = cur.getString(INDEX_NAZIV);
            int o = cur.getInt(I_OBOJENA);
            String slika = cur.getString(I_SLIKA);
            if (slika != null)
            try {
                slikaURL = new URL(slika);
            }
            catch (MalformedURLException e) {

            }

            //Mogu bit null
            String opis = cur.getString(INDEX_OPIS);
            String datum = cur.getString(INDEX_DATUM);
            int brojStr = cur.getInt(INDEX_BROJ);


            long id = cur.getLong(INDEX); //Indeks knjige u tabeli Knjiga
            Log.d ("indeks", Long.toString(id));

            //Pronadjimo autore
            ArrayList<Autor> autori = new ArrayList<>();

            //Upit vraca sve idautora s trazenim idKnjige iz Autorstvo
            String[] koloneRezultatK = new String[] {DBOpenHelper.AUTORSTVO_IDAUTORA, DBOpenHelper.AUTORSTVO_IDKNJIGE};
            String whereA = DBOpenHelper.AUTORSTVO_IDKNJIGE + "= " + id ;
            Cursor curA = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneRezultatK, whereA, whereArgs, groupBy, having, order);

            int INDEX_Autor = curA.getColumnIndexOrThrow(DBOpenHelper.AUTORSTVO_IDAUTORA);
            while(curA.moveToNext()) {
                int idAutor = curA.getInt(INDEX_Autor);

                //Pronadjimo autora u tabeli Autor
                String[] kolonaRezultatA = new String[] {DBOpenHelper.AUTOR_ID, DBOpenHelper.AUTOR_IME};
                String whereAut = DBOpenHelper.AUTOR_ID + "= " + idAutor;
                Cursor curAut = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTOR, kolonaRezultatA, whereAut, whereArgs, groupBy, having, order);
                int index_ime = curAut.getColumnIndexOrThrow(DBOpenHelper.AUTOR_IME);

                while (curAut.moveToNext()) {
                    String ime = curAut.getString(index_ime);
                    Autor aut = new Autor(ime, idws);
                    autori.add(aut);
                }

                curAut.close();
            }
            curA.close();

            //(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica
            Knjiga k = new Knjiga(idws, naziv, autori, opis, datum, null, brojStr);
            if (slikaURL!=null)   k = new Knjiga(idws, naziv, autori, opis, datum, slikaURL, brojStr);

            if (o==1) {
                Log.d("obojena", k.getNaziv());
                k.setObojena(true);
            }
            rez.add(k);
        }

        cur.close();
        return rez;
    }


    ArrayList<Knjiga> knjigeAutora(long idAutor) {
        ArrayList<Knjiga> rez = new ArrayList<>();
        //Vraca knjige datog autora
        //Iz tabele Autorstvo pokupimo idKnjige, iz tabele Knjige pokupimo info o knjizi, provjera da li ima jos autora koji su napisali tu knjigu
        String[] koloneRezultat = new String[] {DBOpenHelper.AUTORSTVO_IDAUTORA, DBOpenHelper.AUTORSTVO_IDKNJIGE};

        String where = DBOpenHelper.AUTORSTVO_IDAUTORA + "= " + idAutor, whereArgs[] = null, groupBy = null, having = null, order = null;
        Cursor cur = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneRezultat, where, whereArgs, groupBy, having, order);

        int INDEX_ID_KNJIGE = cur.getColumnIndexOrThrow(DBOpenHelper.AUTORSTVO_IDKNJIGE);

        while (cur.moveToNext()) {
            int idKnjige = cur.getInt(INDEX_ID_KNJIGE);

            //Trazimo knjigu u tabeli knjige

            String[] koloneRezultatK = new String[] {DBOpenHelper.KNJIGA_ID, DBOpenHelper.KNJIGA_NAZIV, DBOpenHelper.KNJIGA_ID_WS, DBOpenHelper.KNJIGA_BR_STR, DBOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA, DBOpenHelper.KNJIGA_OPIS, DBOpenHelper.KNJIGA_PREGLEDANA, DBOpenHelper.KNJIGA_SLIKA};
            where = DBOpenHelper.KNJIGA_ID + "= " + idKnjige;
            Cursor curK = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KNJIGA, koloneRezultatK, where, whereArgs, groupBy, having, order);

            int INDEX_ID_WS = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_ID_WS);
            int INDEX_NAZIV = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_NAZIV);
            int INDEX_OPIS = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_OPIS);
            int INDEX_DATUM = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA);
            int INDEX_BROJ = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_BR_STR);
            int INDEX = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_ID);
            int I_OBOJENA = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_PREGLEDANA);
            Log.d("index", Integer.toString(I_OBOJENA));
            int I_SLIKA = curK.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_SLIKA);

            while (curK.moveToNext()) {
                String idws = curK.getString(INDEX_ID_WS);
                String naziv = curK.getString(INDEX_NAZIV);
                String opis = curK.getString(INDEX_OPIS);
                String datum = curK.getString(INDEX_DATUM);
                int brojStr = curK.getInt(INDEX_BROJ);
                long id = curK.getLong(INDEX);
                int o = curK.getInt(I_OBOJENA);
                String slika = curK.getString(I_SLIKA);
                try {
                    slikaURL = new URL(slika);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                //Pronadjimo autore
                ArrayList<Autor> autori = new ArrayList<>();
                //Upit vraca sve idautora s trazenim idKnjige
                String[] koloneRezultatA = new String[] {DBOpenHelper.AUTORSTVO_IDKNJIGE, DBOpenHelper.AUTORSTVO_IDAUTORA};
                String whereA = DBOpenHelper.AUTORSTVO_IDKNJIGE + "= " + id;
                Cursor curA = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneRezultatA, whereA, whereArgs, groupBy, having, order);

                int INDEX_Autor = curA.getColumnIndexOrThrow(DBOpenHelper.AUTORSTVO_IDAUTORA);
                while(curA.moveToNext()) {
                    int idAutora = curA.getInt(INDEX_Autor);

                    //Pronadjimo autora u tabeli Autor
                    String[] kolonaRezultatA = new String[] {DBOpenHelper.AUTOR_ID, DBOpenHelper.AUTOR_IME};
                    String whereAut = DBOpenHelper.AUTOR_ID + "= " + idAutora;
                    Cursor curAut = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTOR, kolonaRezultatA, whereAut, whereArgs, groupBy, having, order);
                    int index_ime = curAut.getColumnIndexOrThrow(DBOpenHelper.AUTOR_IME);
                    while (curAut.moveToNext()) {
                        String ime = curAut.getString(index_ime);
                        Autor aut = new Autor(ime, idws);
                        autori.add(aut);
                    }

                    curAut.close();

                }

                curA.close();

                //(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica
                Knjiga k = new Knjiga(idws, naziv, autori, opis, datum, slikaURL, brojStr);
                if (o==1) k.setObojena(true);
                rez.add(k);
            }
            curK.close();
        }

        cur.close();

        return rez;
    }
}
