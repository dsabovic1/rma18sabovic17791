package ba.unsa.etf.rma.dzenana.mybooks;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.net.URL;

/**
 * Created by Dyenana on 05/13/2018.
 */

public class KnjigePoznanika extends IntentService {
    public static final int STATUS_START=0, STATUS_FINISH=1, STATUS_ERROR=2; //deklaracija konstanti
    Bundle bundle = new Bundle();

    //Konstruktori

    public KnjigePoznanika() { //Podrazumjevani konstruktor
        super(null);
    }
    public KnjigePoznanika(String name) {
        super(name);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        //akcije pri kreiranju servisa
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //kod u posebnoj niti
        //funkcionalnost servisa koja je vremenski zahtjevna
        ArrayList<Knjiga> listaKnjiga = new ArrayList<Knjiga>();
        //Primamo intent i sve proslijedjene podatke
        String idKorisnika = intent.getStringExtra("idKorisnika");
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        receiver.send(STATUS_START, Bundle.EMPTY);
        Log.v("intentservis","Intent servis pokrenut");

        //Poziv Google books
        String queryId = null;
        try {
            queryId = URLEncoder.encode(idKorisnika, "utf-8");
        }
        catch (UnsupportedEncodingException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(2, bundle);
            e.printStackTrace();
        }

        String url1 = "https://www.googleapis.com/books/v1/users/" + queryId + "/bookshelves";
        //String url1 = "https://www.googleapis.com/books/v1/users/113210132956130760181/bookshelves";
        ArrayList<String> bookshelfID = new ArrayList<String>();
        String newId = "";
        //korisnik:113210132956130760181
        JSONArray items = new JSONArray();
        try {
            URL url = new URL(url1);
            //pozivanje web servisa
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = Konvertori.convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            items = jo.getJSONArray("items");
        }
        catch (MalformedURLException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(2, bundle);
            e.printStackTrace();
        }
        catch (JSONException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(2, bundle);
            e.printStackTrace();
        }
        catch (IOException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(2, bundle);
            e.printStackTrace();
        }

        for (int i=0; i<items.length(); i++) {
            try {
                JSONObject item = items.getJSONObject(i);
                newId = item.getString("id"); //id je int?
                String access = item.getString("access");
                //Uzimamo bookshelf id za public bookself
                if (access.equalsIgnoreCase("PUBLIC")) {
                    bookshelfID.add(newId); //dodajemo id-eve public bookshelfa
                    Log.d("bookshelfID", newId);
                }


            } catch (JSONException e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(2, bundle);
                e.printStackTrace();
            }

        }
        listaKnjiga  = new ArrayList<Knjiga>();
        for (int b = 0; b < bookshelfID.size(); b++) {
            String queryBookshelfId = null;
            try {
                queryBookshelfId = URLEncoder.encode(bookshelfID.get(b).toString(), "utf-8");
            }
            catch (UnsupportedEncodingException e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(2, bundle);
                e.printStackTrace();
            }
            String url2 = "https://www.googleapis.com/books/v1/users/" + idKorisnika + "/bookshelves/" + queryBookshelfId  + "/volumes";
            Log.d("bookshelfID", queryBookshelfId);


            items = new JSONArray();
            try {
                URL url = new URL(url2);
                //pozivanje web servisa
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = Konvertori.convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                items = jo.getJSONArray("items");
            }
            catch (MalformedURLException e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(2, bundle);
                e.printStackTrace();
            }
            catch (JSONException e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(2, bundle);
                e.printStackTrace();
            }
            catch (IOException e) {
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(2, bundle);
                e.printStackTrace();
            }

            for (int i=0; i<items.length(); i++) {
                try {

                    JSONObject item = items.getJSONObject(i);
                    String id = item.getString("id");
                    JSONObject podaci = item.getJSONObject("volumeInfo");

                    String naziv = "", datum = "" ,opis = "", autor = "";
                    int brStranica = 0;
                    JSONArray autori = new JSONArray();
                    try {
                        naziv = podaci.getString("title");
                        autori = podaci.getJSONArray("authors");
                        for (int k = 0; k < autori.length(); k++) {
                            autor = autori.getString(k);
                        }
                        datum = podaci.getString("publishedDate");
                        opis = podaci.getString("description");
                        brStranica = podaci.getInt("pageCount");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Knjiga novaKnjiga = new Knjiga(naziv, autor, "kategorija");
                    novaKnjiga.setId(id);
                    novaKnjiga.setOpis(opis);
                    novaKnjiga.setDatumObjavljivanja(datum);
                    novaKnjiga.setBrojStranica(brStranica);
                    listaKnjiga.add(novaKnjiga); //Dodajemo novu knjigu


                } catch (JSONException e) {
                    bundle.putString(Intent.EXTRA_TEXT, e.toString());
                    receiver.send(2, bundle);
                    e.printStackTrace();
                }

            }

        }
        //Saljemo rezultat
        bundle.putParcelableArrayList("resultData", listaKnjiga);
        receiver.send(STATUS_FINISH, bundle);

    }


}
