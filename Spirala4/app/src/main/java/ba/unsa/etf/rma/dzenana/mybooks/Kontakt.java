package ba.unsa.etf.rma.dzenana.mybooks;

/**
 * Created by Dyenana on 05/25/2018.
 */

public class Kontakt {
    String name, email;

    public Kontakt(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
