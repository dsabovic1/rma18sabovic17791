package ba.unsa.etf.rma.dzenana.mybooks;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dyenana on 04/09/2018.
 */
public class ListAdapter extends ArrayAdapter<Knjiga> {

    //Interfejs za komunikaciju sa mainActivity
    public interface ListAdapterListener {
        void openFragmentPreporuci(Knjiga k);
    }

    private ListAdapterListener m_listener;


   /* public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        m_listener = (ListAdapterListener) context;
    }*/


    public ListAdapter(Context context, int resource, List<Knjiga> items) {
        super(context, resource, items);
        m_listener = (ListAdapterListener) context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;


        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.element_liste, null);
        }

        final Knjiga k = getItem(position);

        if (k != null) {
            TextView eNaziv = (TextView) v.findViewById(R.id.eNaziv);
            TextView eAutor = (TextView) v.findViewById(R.id.eAutor);
            TextView eDatumObjavljivanja = (TextView) v.findViewById(R.id.eDatumObjavljivanja);
            TextView eOpis = (TextView) v.findViewById(R.id.eOpis);
            TextView eBrojStranica = (TextView) v.findViewById(R.id.eBrojStranica);
            Button dPreporuci = (Button) v.findViewById(R.id.dPreporuci);

            dPreporuci.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    //Otvaranje fragmenta FragmentPreporuci, prosljedjujemo Knjigu pomocu interfejsa
                    Log.d("Knjiga", k.getNaziv());
                    Log.d("Knjiga", k.getAutori().get(0).getImeiPrezime());
                    m_listener.openFragmentPreporuci(k);
                }
            });

            ImageView eNaslovna = v.findViewById(R.id.eNaslovna);

            if (eNaziv != null) {
                eNaziv.setText("Naziv: " + k.getNaziv());
            }

            if (eAutor != null) {
                String autori = "Autor/i:";
                ArrayList<Autor> a = k.getAutori();
                for (int i=0; i<a.size(); i++) {
                    if (i!=0) autori+= ", ";
                    autori+= a.get(i).getImeiPrezime();
                }
                eAutor.setText(autori);
            }

            if (eDatumObjavljivanja!=null) {
                String datum = k.getDatumObjavljivanja();
                if (datum!= null) {
                    eDatumObjavljivanja.setText("Datum objavljivanja: " +datum);
                }
                else  eDatumObjavljivanja.setText("");

            }

            if (eOpis!=null) {
                String opis = k.getOpis();
                if (opis!= null) {
                    eOpis.setText("Opis: " +opis);
                }
                else  eOpis.setText("");

            }

            if (eBrojStranica!=null) {
                int br = k.getBrojStranica();
                if (br!= 0) {
                    eBrojStranica.setText("Br. stranica: " + Integer.toString(br));
                }
                else  eBrojStranica.setText("");

            }


            if (k.isObojena()) {
                v.setBackgroundColor(v.getResources().getColor(R.color.svijetloPlava));
            }


            URL url = k.getSlika();
            if (url== null) {
                Log.d("slika", "url null");
                Picasso.get().load("https://books.google.ba/googlebooks/images/no_cover_thumb.gif").into(eNaslovna);
            }
            else {
                String s = url.toString();
                Picasso.get().load(s).into(eNaslovna);
                Log.d("slikaAdapter", s);
            }
    }

        return v;
    }

}

