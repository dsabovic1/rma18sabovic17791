package ba.unsa.etf.rma.dzenana.mybooks;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Dyenana on 04/07/2018.
 */

public class ListAutorAdapter extends ArrayAdapter<Autor> {

    public ListAutorAdapter(@NonNull Context context, ArrayList<Autor> items)
    {
        super(context, R.layout.element_liste_autor ,items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater vi = LayoutInflater.from(getContext());
        View v = vi.inflate(R.layout.element_liste_autor, parent, false);

        Autor a = getItem(position);

        if (a != null) {
            TextView imePrezimeAutoraTextView = (TextView) v.findViewById(R.id.imePrezimeAutoraTextView);
            TextView brojKnjigaTextView = (TextView) v.findViewById(R.id.brojKnjigaTextView);
            TextView brojKnjigaTextLabel = (TextView) v.findViewById(R.id.brojKnjigaTextLabel);

            if (imePrezimeAutoraTextView != null) {
                imePrezimeAutoraTextView.setText(a.getImeiPrezime());
            }

            if (brojKnjigaTextView != null) {
                //Prebrojimo knjige autora - novi upit
                int brojKnjiga = 0, idAutora = 0;

                String[] koloneRezultat = new String[] {DBOpenHelper.AUTOR_ID, DBOpenHelper.AUTOR_IME};
                String where = DBOpenHelper.AUTOR_IME + "= '" + a.getImeiPrezime() + "'";
                String  whereArgs[] = null, groupBy = null, having = null, order = null;
                Cursor c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTOR, koloneRezultat, where, whereArgs, groupBy, having, order);

                int INDEX = c.getColumnIndexOrThrow(DBOpenHelper.AUTOR_ID);
                while (c.moveToNext()) {
                    idAutora = c.getInt(INDEX);
                }
                c.close();

                koloneRezultat = new String[] {DBOpenHelper.KNJIGA_ID, DBOpenHelper.AUTOR_ID};
                where = DBOpenHelper.AUTOR_ID + "= " + idAutora;
                c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneRezultat, where, whereArgs, groupBy, having, order);

                brojKnjiga = c.getCount();
                brojKnjigaTextView.setText(String.valueOf(brojKnjiga));
            }

            if (brojKnjigaTextLabel != null) {
                brojKnjigaTextLabel.setText(R.string.brojKnjigaTextLabel);
            }


        }

        return v;
    }
}
