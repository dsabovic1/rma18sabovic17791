package ba.unsa.etf.rma.dzenana.mybooks;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Dyenana on 03/29/2018.
 */

public class ListaObjekata {
    public static ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
    public static ArrayList<String> kategorije = new ArrayList<String>();
    public static ArrayList<Autor> autori = new ArrayList<Autor>();
}
