package ba.unsa.etf.rma.dzenana.mybooks;

import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import static ba.unsa.etf.rma.dzenana.mybooks.ListaObjekata.autori;
import static ba.unsa.etf.rma.dzenana.mybooks.ListaObjekata.kategorije;
import static ba.unsa.etf.rma.dzenana.mybooks.ListaObjekata.knjige;

/**
 * Created by Dyenana on 04/05/2018.
 */

public class ListeFragment extends android.support.v4.app.Fragment {
    ArrayAdapter<String> adapterKategorija;
    ListAutorAdapter adapterAutor;
    static Boolean prvi = true;
    String odabir;
    char co;
    DBOpenHelper helper = new DBOpenHelper(getContext());

    android.support.v4.app.FragmentManager fm;
    android.support.v4.app.FragmentTransaction ft;

    Button dAutori;
    Button dKategorije;
    Button dDodajKategoriju;
    Button dDodajKnjigu;
    Button dPretraga;
    Button dDodajOnline;
    EditText tekstPretraga;

    ListeFragmentListener  acticityCommander;

    public interface ListeFragmentListener {
        void addKnjigeFragment(String odabrana, char c);
        void addDodavanjeKnjigeFragment();
        void addOnlineFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            acticityCommander = (ListeFragmentListener) context;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.liste_fragment);
        //Mehanizam dinamičkog ubacivanja fragmenata
        //dohvatanje FragmentManagera
        fm = getFragmentManager();

        //Uzimamo kategorije iz baze
        ListaObjekata.kategorije.clear();

        String[] koloneRezultat = new String[] {DBOpenHelper.KATEGORIJA_ID, DBOpenHelper.KATEGORIJA_NAZIV};
        String where = null, whereArgs[] = null, groupBy = null, having = null, order = null;
        Cursor c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, koloneRezultat, where, whereArgs, groupBy, having, order);

        //Prolazimo kroz kategorije i uzimamo nazive i id
        int INDEX_NAZIV = c.getColumnIndexOrThrow(DBOpenHelper.KATEGORIJA_NAZIV);
        while (c.moveToNext()) {
            ListaObjekata.kategorije.add(c.getString(INDEX_NAZIV));
            Log.d("Kategorija:", c.getString(INDEX_NAZIV));
        }

        //Uzimamo autore iz baze (samo ime i listu idKnjige)
        ListaObjekata.autori.clear();
        ArrayList<String> knjigeAutora = new ArrayList<>();
        String ime;

        String[] koloneRezultatA = new String[] {DBOpenHelper.AUTOR_ID, DBOpenHelper.AUTOR_IME};
        Cursor cur = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTOR, koloneRezultatA, where, whereArgs, groupBy, having, order);

        int INDEX_NAZIV_A = cur.getColumnIndexOrThrow(DBOpenHelper.AUTOR_IME);
        int INDEX_ID_A = cur.getColumnIndexOrThrow(DBOpenHelper.AUTOR_ID);
        while (cur.moveToNext()) {
            ime = cur.getString(INDEX_NAZIV_A);
            int idAutora = cur.getInt(INDEX_ID_A);
            //Pronadjimo sve knjige autora

            //Pronadjimo idKnjige u tabeli Autorstvo
            String[] koloneRezultatAut = new String[] {DBOpenHelper.AUTORSTVO_IDKNJIGE, DBOpenHelper.AUTORSTVO_IDAUTORA};
            String whereA =  DBOpenHelper.AUTORSTVO_IDAUTORA + "= " + idAutora ;
            Cursor cursor = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_AUTORSTVO, koloneRezultatAut, whereA, whereArgs, groupBy, having, order);

            int INDEX = cursor.getColumnIndexOrThrow(DBOpenHelper.AUTORSTVO_IDKNJIGE);
            while (cursor.moveToNext()) {
                int idKnjige = cursor.getInt(INDEX);

                //Pronadjimo knjigu u tabeli Knjiga i dodajmo je autoru (dodajemo id_ws knjige)
                String id_ws = pronadjiKnjigu(idKnjige);
                knjigeAutora.add(id_ws);
            }
            cursor.close();
            if (knjigeAutora.size()>0) {
                Autor a = new Autor(ime, knjigeAutora.get(0));
                for (int i=1; i<knjigeAutora.size(); i++) {
                    a.dodajKnjigu(knjigeAutora.get(i));
                }
                ListaObjekata.autori.add(a);
            }
        }
        cur.close();




    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.liste_fragment, container, false);
        dAutori = (Button) view.findViewById(R.id.dAutori);
        dKategorije = (Button) view.findViewById(R.id.dKategorije);
        dDodajKategoriju = (Button) view.findViewById(R.id.dDodajKategoriju);
        dPretraga = (Button) view.findViewById(R.id.dPretraga);
        dDodajKnjigu = (Button) view.findViewById(R.id.dDodajKnjigu);
        tekstPretraga = (EditText) view.findViewById(R.id.tekstPretraga);
        dDodajKategoriju.setEnabled(false); //Onemogućeno dugme
        dDodajOnline = (Button) view.findViewById(R.id.dDodajOnline);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final ListView lista = (ListView) view.findViewById(R.id.lista);

        dDodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDodavanjeKnjigeFragment();
            }
        });

        dAutori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Skrivanje dugmića
                dPretraga.setVisibility(View.GONE);
                dDodajKategoriju.setVisibility(View.GONE);
                tekstPretraga.setVisibility(View.GONE);
                //Ucitati autore u ListView
                //Autori
                adapterAutor = new ListAutorAdapter(getContext(), ListaObjekata.autori);
                for (int i=0; i<ListaObjekata.autori.size(); i++) {
                    Autor a = ListaObjekata.autori.get(i);
                    Log.d("Autori", String.valueOf(a.brojKnjiga()));
                }

                //Povezivanje adaptera sa listom
                lista.setAdapter(adapterAutor);

                lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                        TextView tv = (TextView) view.findViewById(R.id.imePrezimeAutoraTextView);
                        odabir = tv.getText().toString();
                        if (odabir == null) {
                            //Nema autora
                            odabir = "N/A";
                        }
                        Toast.makeText(getContext(), odabir, Toast.LENGTH_SHORT);
                        co = 'a'; //Oabrani autori
                        addKnjigeFragment(view);
                    }
                });

            }
        });

        dDodajOnline.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addOnlineFragment();
            }
        });

        dKategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            dPretraga.setVisibility(View.VISIBLE);
            dDodajKategoriju.setVisibility(View.VISIBLE);
            tekstPretraga.setVisibility(View.VISIBLE);


            //Ucitati kategorije u ListView
            adapterKategorija = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_list_item_1, kategorije);

            //Povezivanje adaptera sa listom
            lista.setAdapter(adapterKategorija);


            dDodajKategoriju.setEnabled(false); //Onemogućeno dugme


            dPretraga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String k = tekstPretraga.getText().toString();
                    //Filtrirajmo listu
                    adapterKategorija.getFilter().filter(k);

                    //Rezultat je prazna lista
                    if (!kategorije.contains(k) && tekstPretraga.getText().length() > 0) {
                        dDodajKategoriju.setEnabled(true); //Omogućeno dugme
                    } else {
                        dDodajKategoriju.setEnabled(false);
                    }

                }
            });

            dDodajKategoriju.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    long r = dodajKategoriju(tekstPretraga.getText().toString());
                    if (r == -1)  {
                        Toast.makeText(getContext(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (r == -2)  {
                        Toast.makeText(getContext(), "Kategorija vec postoji!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    adapterKategorija.clear();
                    adapterKategorija.addAll(kategorije);
                    adapterKategorija.notifyDataSetChanged(); //Adapter obavjestavamo da je doslo do promjene
                    tekstPretraga.setText(""); //Vraćamo na prazno
                    dDodajKategoriju.setEnabled(false);
                }
            });

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                    odabir = (String) (lista.getItemAtPosition(i));
                    co = 'k'; //Oabrane kategorije
                    addKnjigeFragment(view);

                }
            });


            }
        });

    }

    private long dodajKategoriju(String naziv) {
        //Provjerimo da li kategorija postoji u bazi
        Cursor c;
        try {
            String[] koloneRezultat = new String[] {DBOpenHelper.KATEGORIJA_ID, DBOpenHelper.KATEGORIJA_NAZIV};
            String where = DBOpenHelper.KATEGORIJA_NAZIV + " = '" + naziv+"'", whereArgs[] = null, groupBy = null, having = null, order = null;
            c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, koloneRezultat, where, whereArgs, groupBy, having, order);

            if (c.getCount() >0) return -2; //Ima je vec

            c.close();

        }
        catch (SQLiteException e) {
            Log.d("sql", "Greska1");
            return -1;
        }

        //Upisimo novu kategoriju u bazu, ne postoji
        ContentValues nova = new ContentValues();
        nova.put(DBOpenHelper.KATEGORIJA_NAZIV, naziv);
        KategorijeAkt.db.insert(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, null, nova);
        //Dodajemo u listu kategorija, azuriramo je stalno
        ListaObjekata.kategorije.add(naziv);

        //Nadjimo id koji trebamo vratiti
        try {
            String[] koloneRezultat = new String[] {DBOpenHelper.KATEGORIJA_ID, DBOpenHelper.KATEGORIJA_NAZIV};
            String where = DBOpenHelper.KATEGORIJA_NAZIV + " = '" + naziv+ "'", whereArgs[] = null, groupBy = null, having = null, order = null;
            c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KATEGORIJA, koloneRezultat, where, whereArgs, groupBy, having, order);

            //Prolazimo kroz kategorije i uzimamo nazive i id
            int INDEX_ID = c.getColumnIndexOrThrow(DBOpenHelper.KATEGORIJA_ID);
            int id = -1;

            while (c.moveToNext()) {
                id = c.getInt(INDEX_ID);
                Log.d("ID", String.valueOf(id));
            }

            c.close();

            return id;

        }
        catch (SQLiteException e) {
            Log.d("sql", "Greska2");
            return -1;

        }


    }

    private void addDodavanjeKnjigeFragment() {
        acticityCommander.addDodavanjeKnjigeFragment();
    }

    private void addKnjigeFragment(View v) {
        acticityCommander.addKnjigeFragment(odabir, co);
    }

    private void addOnlineFragment() {acticityCommander.addOnlineFragment();}

    public String pronadjiKnjigu(int idKnjige) {
        String id_ws="";
        String[] koloneRezultat = new String[] {DBOpenHelper.KNJIGA_ID, DBOpenHelper.KNJIGA_ID_WS};
        String where = DBOpenHelper.KNJIGA_ID + " = " + idKnjige, whereArgs[] = null, groupBy = null, having = null, order = null;
        Cursor c = KategorijeAkt.db.query(DBOpenHelper.DATABASE_TABLE_KNJIGA, koloneRezultat, where, whereArgs, groupBy, having, order);
        int INDEX = c.getColumnIndexOrThrow(DBOpenHelper.KNJIGA_ID_WS);
        while (c.moveToNext()) {
            id_ws = c.getString(INDEX);
        }
        c.close();

        return id_ws;
    }


}




